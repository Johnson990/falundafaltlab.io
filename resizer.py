from PIL import Image
from sys import argv

def resize_image(image_name, x=800, y=600):
    with Image.open(image_name) as im:
        im_resized = im.resize((x, y), reducing_gap=5000000)
        base, fname = image_name.rsplit("/", 1)
        im_resized.save(f"{base}/resized_{fname}")
        #  im_resized.show()

if __name__ == "__main__":
    dimensions = argv[1]
    x, y = (int(l) for l in dimensions.split("x"))
    for image_name in argv[2:]:
        resize_image(image_name, x, y)
