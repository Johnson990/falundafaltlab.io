import logging
from settings import CONTENT_SERVER

logger = logging.getLogger(__name__)

class StringBuilder:
    def __init__(self):
        self.stack = []
        self.parts = []

    def __str__(self):
        while self.stack:
            logger.warning(f"found unclosed tag {self.stack[-1]}")
            self.close_tag()
        return "".join(self.parts)

    def open_tag(self, tag, tag_id="", cls=""):
        if tag_id and cls:
            self.parts.append(f"<{tag} id='{tag_id}' class='{cls}'>")
        elif tag_id:
            self.parts.append(f"<{tag} id='{tag_id}'>")
        elif cls:
            self.parts.append(f"<{tag} class='{cls}'>")
        else:
            self.parts.append(f"<{tag}>")
        self.stack.append(tag)
        return self

    def close_tag(self):
        tag = self.stack.pop()
        self.parts.append(f"</{tag}>")
        return self

    def add_text(self, text):
        """
        Todo: sanitize input
        """
        self.parts.append(text)
        return self

def read(file: str):
    """I should still obviously use jinja"""
    with open(file, "r") as f:
        return f.read().replace("{CONTENT_SERVER}", CONTENT_SERVER)
