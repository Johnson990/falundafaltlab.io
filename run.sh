#!/usr/bin/env bash
set -e
PYTHONPATH=$PWD
websocketd --port 5000 --ssl --sslcert=/etc/letsencrypt/live/peterisratnieks.id.lv/fullchain.pem --sslkey=/etc/letsencrypt/live/peterisratnieks.id.lv/privkey.pem --maxforks=100 ./server.py
# websocketd --port 1443 --redirport 1080 --ssl --sslcert=/etc/letsencrypt/live/peterisratnieks.id.lv/fullchain.pem --sslkey=/etc/letsencrypt/live/peterisratnieks.id.lv/privkey.pem --staticdir public --maxforks=1000 ./server.py
# websocketd --port 1443 --redirport 1080 --staticdir static ./server.py
