\pagenumbering{gobble}
# Informacija apie Falun Dafa persekiojimą Kinijoje
Kinijos komunistų partija (KKP) yra pagrindinė žmogaus teisių pažeidėja.
Tai beveik visuotinai žinoma.
Daugelis mano, kad tai yra tik Kinijos problemos ir neturi nieko bendro su
su Vakarais ar konkrečiai su Lietuva. Toks supratimas nėra
teisingas. Daugelis mano, kad tai neturi nieko bendro su jais asmeniškai.
Tai lemia kolektyvinio neatsakingumo būseną.

## KKP yra nusikalstama organizacija
Būdama nusikalstama organizacija, jos įtaka peržengia valstybių sienas.
Jos nariai nebūtinai yra kinai.
Ji veikia ne kinų tautos labui.
Tie, kurie daro nusikaltimus arba dangsto nusikaltimus KKP naudai
veikia dėl savanaudiškų finansinių ar politinių interesų -- kenkia 
kitiems ir daro didelę žalą.
Siekiant naudos tautai juos reikia demaskuoti.
Žinoma, KKP nėra vienintelis nusikaltėlis -- yra daug blogų žmonių.
Tačiau net ir tarp nusikaltėlių yra moralinių stabdžių.
Pavyzdžiui, dauguma nusikaltėlių nežudytų.
Tačiau KKP nesustoja prieš bet kokį blogį.
Ji yra labiausiai manipuliuojanti, žiauriausia, klastingiausia ir labiausiai 
patyrusi nusikaltimų vykdytoja.
Būtina atkreipti dėmesį į jos veiksmus.

## Falun Dafa praktikuotojų kankinimo metodai
Citata iš knygos „Devyni komentarai apie komunistų partiją“:

> Jie verčia Falun Gong praktikuotojus dėvėti „bepročio liemenes“, tada sukryžiuoja ir suriša rankas už nugaros. Jie iškelia rankas per pečius iki krūtinės priekio, suriša praktikuotojų kojas ir pakabina juos už lango. Tuo pat metu jie užkemša praktikuotojams burnas skuduru, į ausis kiša ausines ir nuolat leidžia Falun Gong šmeižiančius pranešimus. Liudininkų pasakojimu, taip kankinamiems žmonėms greitai lūžta rankos, sausgyslės, pečiai, riešai ir alkūnės. Ilgą laiką taip kankinamiems žmonėms lūžta stuburas ir jie miršta kankinančiame skausme.
>
> Be to, kankinius meta į požemius, pripildytus nuotekų. Po praktikuotojų nagais įkiša bambukines lazdeles ir verčia gyventi drėgnose patalpose, kuriose ant lubų, grindų ir sienų pilna raudonų, žalių, geltonų, baltų ir kitokių pelėsių, dėl kurių jų sužalojimai pūliuoja. Be to, praktikuotojus kandžioja šunys, gyvatės ir skorpionai, jie švirkščia praktikuotojams nervus žalojančius vaistus. Tai tik keletas būdų, kuriais praktikuotojai kankinami darbo stovyklose.

## Priverstinis organų paėmimas
Vienas baisiausių nusikaltimų -- organų paėmimas iš „mirties bausme nuteistų kalinių“.
Iš tikrųjų tai dažniausiai būna sąžinės kaliniai, kurie net nėra padarę jokių nusikaltimų.
Jie laikomi kaip gyvų organų bankas ir žudomi pagal poreikį siekiant pelno.
Tai daroma pramoniniu mastu.
Pirmąją sistemingą ataskaitą apie tai 2006 m. pateikė Ethanas Gutmannas.
Nuo to laiko tai patvirtino daugybė nepriklausomų tyrimų.

## Dabartinė padėtis Lietuvoje yra palyginti gera
2021 m. Lietuvos Seimas pasmerkė KKP nusikaltimus žmoniškumui.
Taip pat Užsienio reikalų ministerijos iniciatyva buvo atidaryta „Taivano atstovybe Lietuvoje“.
Be to, jai buvo leista pasirinkti savo pavadinimą (ir nesivadinti 'Taipėjaus').

Šie du veiksmai neperžengia paprasto naudojimosi žodžio laisve ribų,
tačiau KKP tai nepriimtina ir išprovokavo reakciją, kokios 
galima tikėtis tik iš psichopato.
Netrukus buvo atšaukti ambasadoriai ir nustatyti griežti prekybos apribojimai. 
Dėl to Lietuva trumpam atsidūrė pasaulio politinės scenos centre.
Tai rodo, kaip svarbu netylėti šiais klausimais.
KKP bijo, kad tiesa apie jos nusikalstamą prigimtį iškils į viešumą.
Taip pat akivaizdu, kad KKP nėra suinteresuota dialogu ir
neketina keistis.
Istorija parodė, kad abipusiai naudinga partnerystė su KKP yra 
neįmanoma, kadangi ji sistemingai išdavinėjo savo partnerius, kai galėjo 
iš to gauti naudos.
Kito savarankiškumas nepripažįstamas, o galutinis bet kokias
partnerystės tikslas yra vergovė.
Tie, kurie vis dar veda dialogą su KKP atstovais, yra arba to
nežinantys, arba žaidžia politinį teatrą.

## Trumpa analizė
Paanalizuokime Seimo balsavimą apie klausimą „Dėl masinių, sistemingų ir sunkių Kinijos vykdomų žmogaus teisių pažeidimų ir uigūrų genocido“.
Turime pagrindo manyti, kad tą dieną posėdyje dalyvavo mažiausiai 135 nariai.
Tik 94 nusprendė balsuoti dėl šio klausimo.
Tai rodo, kad buvo bandoma pažeisti kvorumą.
Pateiktoje lentelėje parodyta, kaip kiekviena politinė frakcija balsavo dėl šio klausimo.

Frakcija | Už | Prieš | Susilaikė | Registr. | Nedalyvavo
-------------------------------|:-:|:--:|:----:|:----:|:----:|
Darbo partijos frakcija | 1 | 0 | 1 | 0 | 8
Laisvės frakcija | 11 | 0 | 0 | 0 | 0
Liberalų sąjūdžio frakcija | 8 | 0 | 0 | 0 | 4
Lietuvos regionų frakcija | 1 | 1 | 1 | 0 | 6
Lietuvos socialdemokratų partijos frakcija | 9 | 0 | 0 | 0 | 4
Lietuvos valstiečių ir žaliųjų sąjungos frakcija | 12 | 0 | 5 | 2 | 13
Mišri Seimo narių grupė | 2 | 0 | 0 | 0 | 1
Tėvynės sąjungos-Lietuvos krikščionių demokratų frakcija | 42 | 0 | 0 | 0 | 8

## KKP neturėtų būti painiojama su Kinija
Istoriškai kinai buvo taiki, protinga ir darbšti tauta.
Šiuolaikiniai kinai jau gerokai pasikeitę -- egzistuoja daugybę priežasčių jų nemėgti.
Tai lemia sistemingas jų tradicinės kultūros naikinimas 
ir ilgalaikė ideologinė indoktrinacija.
Praeityje Lietuva taip pat buvo valdoma komunistų.
Vyresnioji karta prisimena, kad indoktrinacija prasideda ankstyvoje 
vaikystėje ir tęsiasi tol, kol komunistų partija turi galimybę
kontroliuoti žmones.
Buvo taip, kad per „politinio švietimo“ pamokas mokyklose ir 
universitetuose dėstytojai netikėjo tuo, ko moko, o studentai 
netikėjo tuo, ko juos moko, tačiau neišlaikę šio dalyko
negalėjo baigti mokyklos.

KKP įdeda daug pastangų, kad supainotų sąvokas 'šalis', 'partija' ir 
'tauta'. Kinijos viduje tai daroma tokiais šūkiais: „Mylėk partiją -- 
mylėk tautą“ arba „Lojalumas partijai -- lojalumas šaliai“. 
Už Kinijos ribų mes neturėtume painioti šių dalykų. Iš tikrųjų 
Kinijos tauta yra ta, kuri labiausiai kenčia nuo KKP.

## Ką daryti
Padėti žmogui, atsidūrusiam sunkioje padėtyje, yra geras dalykas -- moralinė pareiga.
Pagalba turėtų būti teikiama vardan tautų draugystės ir 
vardan visuotinių bendrų vertybių, kuriomis dalijasi visa žmonija.

### Sužinokite daugiau
Neleisti žmonėms sužinoti tiesos yra esminis KKP valdžios užgrobimo ir išlaikymo strategijos elementas.
Tai aiškiai įrodo KKP bandymas sučiupti Lietuvą.
Norint suprasti, kaip tiksliai veikia šios strategijos, galima pažvelgti į istoriją.
Šia tema yra daug gerų knygų, pavyzdžiui, „Devyni komentarai apie komunistų partiją“.
Ją nemokamai galite gauti čia: **NineCommentaries.com**.

Minėjome priverstinį organų paėmimą. Buvo atliktas sunkus darbas sistemingai renkant duomenis ir nuorodas.
Jį atliko daugelis garbingų teisės srities visuomenės veikėjų, kurie sudarė nepriklausomą tribunolą, kuriam pirmininkavo seras Geoffrey Nice QC. Daugiau informacijos: **ChinaTribunal.com**.

Apie Falun Dafa egzistuoja daug klaidingų įsitikinimų, kurie dažniausiai skleidžiami internete.
Pavyzdžiui, kad ši grupė yra slapta, kontroversiška ar politiškai motyvuota, prieštarauja mokslui ar medicinai, yra netolerantiška ir fašistinė.
Visa tai yra melas, kuriuo siekiama demonizuoti šią praktiką, kad būtų „įteisintas“ persekiojimas.
Kad išsiaiškintumėte klaidingus įsitikinimus apie šią praktiką, prašome apsilankyti: **FalunInfo.net**.

### Kalbėkite apie visa tai
Visuomenės veikėjai ir žiniasklaidos bendrovės, atvirai pasisakantys prieš KKP, dažnai yra tiesiogiai puolami.

Kai Deividas Matas kalbėjo Australijos radijo laidoje, kuri buvo tiesiogiai transliuojama į Kiniją, klausytojai buvo kviečiami skambinti ir užduoti klausimus. Iš Kinijos paskambinęs vyras sakė: „Aš esu iš interneto policijos. Tai, ką jūs darote, yra išpuolis prieš Kiniją. Įžeidinėdami Kiniją rizikuojate savo gyvybe. Mano klausimas jums: ar nebijote?“

Kai persekiojimas tik prasidėjo, kai kuri tarptautinė žiniasklaida teisingai apie tai pranešė, bet labai greitai nutilo. Po kurio laiko ji netgi pradėjo skleisti KKP propagandą. Manoma, kad šių įmonių vadovai buvo papirkti arba jiems buvo grasinama.

Kalbėdami apie šias problemas, jūs padedate kurti geresnę aplinką. Deividas Matas kartą pasakė: „Niekas taip nedrąsina nusikaltėlio, kaip žinojimas, kad jis gali išvengti bausmės už nusikaltimą“.

### Pasirašykite dokumentą dėl buvusio KKP vadovo patraukimo baudžiamojon atsakomybėn
Tai daugiausia simbolinis veiksmas, tačiau jis turi didelį poveikį, nes padeda kinams atsiriboti nuo KKP.
Kartu jis silpnina KKP.
Tarybiniais laikais net moralinė užsienio šalių parama buvo labai vertinama ir paskatino Tarybų Sąjungos žlungimą.
Šių dokumentų siuntimas į Kiniją yra nuolatinis priminimas, kad šie nusikaltimai yra matomi ir nebus pamiršti.
**en.minghui.org** reguliariai paviešina persekiojimo dalyvių asmeninę informaciją. Ten taip pat publikuojama daug įkvepiančių istorijų apie tai, kaip persekiotojai bijo patekti į šį sąrašą.

### Nepirkite Kinijoje pagamintų produktų
Didelę jų dalį gamina vergai.
Nepirkite jų kaip kalėdinių dovanų -- tai turėtų būti laikoma niekinimu.
Ekonominė nepriklausomybė nuo Kinijos yra labai svarbi šalies stabilumui.
Pavojus ryškiai pasireiškė per pirmąją Vuhano viruso bangą, kai KKP, nejausdama nė menkiausio gėdos jausmo dėl jo atsiradimo, panaudojo pandemiją savo politiniams tikslams įgyvendinti.
<!-- Tuo metu dauguma šalių aptiko, kad jos yra priklausomos nuo Kinijos medicinos prekių tiekimo, todėl KKP nutrauktų tiekimo linijas, jei nebūtų patenkinti jos politiniai reikalavimai. -->

----------------------------------------------------------------------------
|                                        **Daugiau informacijos**

Prašome susisiekti: &nbsp; **petras.racius@inbox.lt** &nbsp; arba &nbsp; **+370 617 22581**

**Šaltiniai**

    https://boycott2008games.blogspot.com/
        2010/07/david-matas-lessons-from-holocaust.html

    https://www.lrs.lt/sip/portal.show?p_r=37067&p_k=1&p_kade_id=9&
        p_ses_id=121&p_fakt_pos_id=-501610&p_bals_id=-42567

