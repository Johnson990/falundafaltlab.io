console.log("loading javascript")
const send = (method, body={}) => {
    console.log(JSON.stringify({method, body}))
    websocket.send(JSON.stringify({method, body}));
}
const send_by_class_name = (class_name, hint=null) => {
    let form_data = Array.from(document.getElementsByClassName(class_name))
        .reduce((data, elem) => {data[elem.id] = elem.value; return data}, {})
    if (hint != null) form_data[hint] = hint
    send(class_name, form_data)
}

const get_function_by_mode = (mode) => {
    if (mode === "replace") {
        return (elem, value) => {elem.innerHTML = value}
    } else if (mode === "append") {
        return (elem, value) => {elem.append(value)}
    } else if (mode === "remove") {
        return (elem, value) => {elem.remove()}
    } else if (mode === "set_path") {
        return (_, value) => {window.location.replace("#" + value)}
    } else if (mode === "submit") {
        return (elem, value) => {
            if (typeof(value) === "string") {
                elem.addEventListener("click", () => {send_by_class_name(value)})
            } else {
                elem.addEventListener("click", () => {send_by_class_name(value[0], value[1])})
            }
        }
    } else if (mode === "click") {
        return (elem, value) => {
            if (typeof(value) === "string") {
                elem.addEventListener("click", () => {send(value)})
            } else {
                elem.addEventListener("click", () => {send(value[0], value[1])})
            }
        }
    } else if (mode === "hide") {
        return (elem, value) => {elem.hidden = value}
    } else if (mode.startsWith("style.")) {
        console.log("styling", mode)
        style_elem = mode.replace("style.", "")
        // console.log(elem)
        console.log(style_elem)
        return (elem, value) => {elem.style[style_elem] = value}
    } else {
        console.warn("unknown mode", mode)
        return (elem, value) => {elem[mode] = value}
    }
}

const get_elem_by_path = (elem, path) => {
    return path.reduce((element, n) => {
        if (n === "<") {
            return element.previousSibling
        } else if (n === ">") {
            return element.nextSibling
        } else if (n === "^") {
            return element.parentElement
        } else if (Number(n) < 0) {
            return element.children[element.children.length - Number(n)]
        } else {
            return element.children[Number(n)]
        }
    }, elem)
}
const load_html = (directives) => {
    console.log("loading")
    directives.forEach((directive) => {
        // console.log(directive)
        let f = get_function_by_mode(directive.mode)
        let specifier = directive.specifier.split(".")
        let elem_id = specifier[0].replace("#", "")
        let path = specifier.splice(1)
        console.log("path", path)
        // console.log("should be by id", specifier)
        if (specifier[0].startsWith("#")) {
            document.getElementsByClassName(elem_id).forEach((elem) => {
                let final_elem = get_elem_by_path(elem, path)
                f(final_elem, directive.value)
            })
        } else {
            // console.log("getting elem")
            let elem = document.getElementById(elem_id)
            console.log("elem", elem)
            let final_elem = get_elem_by_path(elem, path)
            console.log("final_elem", final_elem)
            f(final_elem, directive.value)
        }
    })
}

const load = () => {
    if (window.location.host.endsWith("gitlab.io")) {
        websocket = new WebSocket("wss://peterisratnieks.id.lv:5000");
    } else if (window.location.host.toUpperCase().endsWith("FALUNDAFA.LT")) {
        websocket = new WebSocket("wss://backend.FalunDafa.lt:5000");
    } else {
        websocket = new WebSocket("wss://" + window.location.host);
    }
    websocket.onmessage = function (event) {
        try {
            var data = JSON.parse(event.data);
            switch (data.method) {
                case 'load_html':
                    load_html(data.body)
                    break;
                case 'executive_order':
                    send_by_class_name(data.body.form_name, data.body.hint)
                    break;
                default:
                    console.error("unsupported event", data);
            }
        } catch(err) {
            console.error("failed", data, err);
        }
    };
    websocket.addEventListener("open", () => send("viewport", {"path": window.location.hash}))
    console.log("javascript loaded and initialized")
}
const fallback = () => {
    if (websocket.readyState != 1) {
        window.location.pathname = "fallback.html"
    }
}
setTimeout(load, 10)
setTimeout(fallback, 1000)
// setInterval(ping, 100)
console.log("javascript loaded")
