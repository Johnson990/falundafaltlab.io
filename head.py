import logging
from helpers import read
from functools import wraps

from notifications import notify_signal
from rotations import picture_rotator, declare_rotation
from acordeon import toggle_acordeon, declare_acordeon
from duk_dict import duk_dict
from elements import exercise_picture_directives, around_the_world_directives, attacks_rotation, health_expo_directives, spreading_publicly_rotation
from settings import CONTENT_SERVER

logging.basicConfig()

STATE = {"value": 0}

methods = []
def method(f):
    methods.append(f)
    return f

def method_with_route(route=None, routes={}):
    if route is None:
        return routes
    def decorator(f):
        @wraps(f)
        def new_f():
            return [
                {"mode": "set_path", "specifier": "", "value": route},
                *f(),
            ]
        method(new_f)
        routes[route] = new_f
        return new_f
    return decorator

method(picture_rotator)  # boilerplate?
method(toggle_acordeon)  # boilerplate?

@method
def state_event():
    return {"type": "state", **STATE}


@method_with_route("/")
def default_path():
    return [
        {"mode": "replace", "specifier": "main", "value": read("templates/default_body.html")},
    ]

@method
def viewport(path: str=""):
    path = path if "#" in path else "#"
    #  logging.warn(method_with_route().get(path.split("#")[1], default_path))
    return [
        {"mode": "replace", "specifier": "viewport", "value": read("templates/viewport.html")},
        *method_with_route().get(path.split("#")[1], default_path)(),  # boilerplate
        {"mode": "click", "specifier": "return_home", "value": "viewport"},
        {"mode": "click", "specifier": "view_kas_tai", "value": "view_kas_tai"},
        {"mode": "click", "specifier": "view_kodel", "value": "view_kodel"},
        {"mode": "click", "specifier": "view_istorija", "value": "view_istorija"},
        {"mode": "click", "specifier": "view_about", "value": "view_about"},
        {"mode": "click", "specifier": "view_misconceptions", "value": "view_misconceptions"},
        {"mode": "click", "specifier": "view_music", "value": "view_music"},
        {"mode": "click", "specifier": "view_art", "value": "view_art"},
        {"mode": "click", "specifier": "view_evidence", "value": "view_evidence"},
        {"mode": "click", "specifier": "view_righteous_thoughts", "value": "view_righteous_thoughts"},
    ]


@method_with_route("/kodel_persekiojimas")
def view_kodel():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#969E90"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "center"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/FalunGong_Crackdown_TiananmenAppealsPoliceBrutality-1024x576.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/kodel.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #969E90, White)"},
        #  {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #DEC4B5, White)"},
        #  {"mode": "style.color", "specifier": "header", "value": "FloralWhite"},
    ]


@method_with_route("/kas_tai_Falun_Dafa")
def view_kas_tai():
    duk = declare_acordeon("duk_acordeon", duk_dict)
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#AE6D54"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "bottom"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/Dafa-in-China-Before__Changchun-1024x576.png)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/kas_tai_Falun_Dafa.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #AE6D54, White)"},
        *exercise_picture_directives,
        *around_the_world_directives,
        *duk,
    ]

@method_with_route("/istorija")
def view_istorija():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#E1C0A1"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2013-12-4-minghui-paint-guihang.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/istorija.html")},
        {"mode": "replace", "specifier": "main", "value": read("templates/istorija-2.html")},
        *health_expo_directives,
        *spreading_publicly_rotation,
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #E1C0A1, White)"},
    ]

@method_with_route("/kontaktai")
def view_about():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "center"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2020-12-9-taiwan-paizi_01.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/kontaktai.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
        {"mode": "style.display", "specifier": "sidebar", "value": "flex"},  # display is none for narrow screens
        {"mode": "submit", "specifier": "contact_form_submit", "value": "contact_form"},
    ]

@method_with_route("/nesusipratimai")
def view_misconceptions():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/index.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
        {"mode": "click", "specifier": "misconceptions_kultas", "value": "misconceptions_kultas"},
        {"mode": "click", "specifier": "misconceptions_netolerantiskas", "value": "misconceptions_netolerantiskas"},
        {"mode": "click", "specifier": "misconceptions_vengia_medicinos", "value": "misconceptions_vengia_medicinos"},
        {"mode": "click", "specifier": "misconceptions_slaptas", "value": "misconceptions_slaptas"},
        {"mode": "click", "specifier": "misconceptions_priestaringai_vertinamas", "value": "misconceptions_priestaringai_vertinamas"},
        {"mode": "click", "specifier": "misconceptions_politinis", "value": "misconceptions_politinis"},
        {"mode": "click", "specifier": "misconceptions_Falun", "value": "misconceptions_Falun"},
    ]

@method_with_route("/nesusipratimai/kultas")
def misconceptions_kultas():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/evil_cult.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/nesusipratimai/netolerantiskas")
def misconceptions_netolerantiskas():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/intolerant.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/nesusipratimai/vengia_medicinos")
def misconceptions_vengia_medicinos():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/medicine.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/nesusipratimai/slaptas")
def misconceptions_slaptas():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/secretive.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
        *attacks_rotation,
    ]

@method_with_route("/nesusipratimai/priestaringai_vertinamas")
def misconceptions_priestaringai_vertinamas():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/contraversial.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/nesusipratimai/politinis")
def misconceptions_politinis():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/political.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/nesusipratimai/Falun")
def misconceptions_Falun():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#D6CCCA"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/2014-1-12-minghui-painting-fx_default.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/misconceptions/Falun.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #D6CCCA, White)"},
    ]

@method_with_route("/muzika")
def view_music():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#45463E"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "center"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/Shen-Yun-Symphony-Orchestra-1.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/music.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #45463E, White)"},
    ]

@method_with_route("/menas")
def view_art():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#49687D"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "top"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/art.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/art.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #49687D, White)"},
    ]

@method_with_route("/irodimai")
def view_evidence():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#000000"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "center"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/tribunal.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/evidence.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #000000, White)"},
    ]

@method
def contact_form(message, contact):
    notify_signal(f"Contact: {contact}\n\n{message}")
    return [
        {"mode": "value", "specifier": "contact_form_submit", "value": "Išsiųsta"},
        {"mode": "disabled", "specifier": "contact_form_submit", "value": True},
    ]


@method_with_route("/teisingos_mintis")
def view_righteous_thoughts():
    return [
        {"mode": "style.backgroundColor", "specifier": "header", "value": "#000000"},
        {"mode": "style.backgroundPosition", "specifier": "header", "value": "center"},
        {"mode": "style.backgroundImage", "specifier": "header", "value": f"url({CONTENT_SERVER}/tribunal.jpg)"},
        {"mode": "replace", "specifier": "main", "value": read("templates/righteous_thoughts.html")},
        {"mode": "style.background", "specifier": "sidebar", "value": "linear-gradient(116deg, #000000, White)"},
    ]
