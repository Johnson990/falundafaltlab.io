from settings import CONTENT_SERVER

duk_dict = {
    "Kokie yra Falun Dafa mokymai ir įsitikinimai?": ["""
Falun Dafa esmė yra vertybės - Tiesa, Gerumas, Kantrybė - (arba kiniškai Džen 真, Šan 善 ir Žen 忍). Falun Dafa moko, kad tai yra pačios svarbiausios visatos savybės. Praktikuotojai stengiasi šiuos principus pritaikyti kasdieniame gyvenime medituodami, atlikdami energetinus pratimus ir studijuodami moralės principus iš Džuan Falun.
    """],
    "Ar Falun Dafa yra religija?": ["""
Norint praktikuoti Falun Dafa, nereikia stoti į jokią organizaciją, nėra narystės, hierarchijos ir mokesčių. Kiekvienas, norintis praktikuoti, gali tai daryti neišleisdamas nė cento, privačiai savo namuose ir nereikalaudamas niekieno pagalbos ar pritarimo.
    """, """
Ši praktika turi dvasinius mokymus ir įsitikinimus, panašius į tuos, kuriuos galima rasti religijoje, ir ji turi kai kuriuos bendrus siekius su grupėmis, kurios oficialiau vadinamos religijomis, įskaitant moralinio savęs išaukštinimo ar dvasinio pabudimo siekį.
    """, """
Pagal daugelio Vakarų šalių įstatymus Falun Dafa laikomas religija arba saugomu tikėjimu. Tačiau Falun Dafa nėra institucionalizuotas; jis neturi jokių oficialių iniciacijos priemonių, oficialaus tikėjimo išpažinimo, pamaldumo praktikos ir panašiai.
    """, """
Falun Dafa nėra nei politinė, nei verslo organizacija. Visą mokomąją medžiagą galima nemokamai rasti internete, o savanoriai taip pat nemokamai veda visus užsiėmimus.
    """],
    "Falun -- Falun Dafa simbolis": [f"""
    </p><div style="display: flex; justify-content: center;"><img src="{CONTENT_SERVER}/Falun.png"></div><p>
    """, """
Falun Dafa emblema vadinama „Falun” - kinų kalbos terminas, kuris laisvai verčiamas kaip „Įstatymo ratas”. Jį sudaro du pagrindiniai elementai: in-jang simboliai, kurie yra daoistinio pobūdžio. Taip pat srivatsa (sanskr.), kurie yra budistiniai. Srivasta, arba svastika, kaip ji vadinama angliškai, dažnai sutinkama Azijos budizme. 
    """, """
Azijos kultūroje neigiamos svastikos konotacijos, siejamos su Hitleriu ir naciais, yra palyginti nežinomos. Iš tikrųjų svastika daugelyje kultūrų turi tūkstantmečius siekiančią istoriją ir yra laikoma laimės simboliu.
    """],
    "Kas praktikuoja Falun Dafa?": ["""
1992 m. Falun Dafa pirmą kartą buvo viešai paskelbtas Kinijoje ir ten plačiai išpopuliarėjo, šią praktiką taip pat pradėjo praktikuoti žmonės visame pasaulyje. Šiandien Falun Dafa praktikuojama daugiau kaip 80 pasaulio šalių. Falun Dafa knygos išverstos į 40 kalbų.
    """],
    "Kiek žmonių praktikuoja Falun Dafa?": ["""
1999 m. pradžioje Kinijos vyriausybės pareigūnai ir valstybinė žiniasklaida teigė, kad Kinijoje Falun Dafa praktikuoja 70-100 mln. žmonių, todėl tuo metu tai buvo sparčiausiai auganti religija pasaulyje. Nors Kinijos komunistų partija pradėjo persekiojimo kampaniją, siekdama „išnaikinti” Falun Dafa, vis dėlto žmogaus teisių organizacijos mano, kad visoje Kinijoje ir toliau praktikuoja dešimtys milijonų žmonių. Falun Dafa praktikuotojų taip pat yra daugiau kaip 100 pasaulio šalių, tačiau dėl to, kad nėra narystės, sunku pateikti tikslius skaičius.
    """],
    "Kas pradėjo Falun Dafa?": ["""
Falun Dafa viešai paskelbė Li Hongzhi. Iš Čančunio (Kinija) kilęs, o dabar JAV gyvenantis Li yra penkis kartus nominuotas Nobelio taikos premijai ir Europos Parlamento nominuotas Sacharovo premijai už minties laisvę. Jis taip pat yra gavęs "Freedom House" Tarptautinį religinės laisvės apdovanojimą.
    """],
    "Kuo skiriasi Falungun ir Falun Dafa?": ["""
„Falungun” yra kitas "Falun Dafa" pavadinimas. Jie reiškia tą patį dalyką - dvasinę praktiką, kilusią iš Kinijos. „Falun Dafa” yra oficialus praktikos pavadinimas, o „Falungun” yra labiau šnekamosios kalbos terminas, išpopuliarėjęs Kinijoje.
    """],
}
