## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
    - echo 'Replace https://manjaro:1443 with https://FalunDafaLt.gitlab.io/falundafainfolt'
    - find ./ -type f -exec sed -i -e 's%https://manjaro:1443%https://FalunDafaLt.gitlab.io/falundafainfolt%g' {} \;
  artifacts:
    paths:
    - public
  only:
  - master
```
