from rotations import declare_rotation

attacks_rotation = declare_rotation(
    "attacks_thuggery",
    [
        "/Attacks-Thuggery-Shooting__SVK.png",
        "/Attacks-Thuggery-Shooting__DavidLiangB.png",
        "/Attacks-Thuggery-Shooting__DavidLiangA.png",
        "/Attacks-Thuggery-Shooting__Flushing-A.png",
        "/Attacks-Thuggery-Shooting__Flushing-B.png",
        "/Attacks-Thuggery-Shooting__Flushing-C.png",
        "/Attacks-Thuggery-Shooting__HK.png",
        "/Attacks-Thuggery-Shooting__LiYuan.png",
        "/Attacks-Thuggery-Shooting__SFchinatown.png",
        "/Attacks-Thuggery-Shooting__Argentina.png",
        "/Attacks-Thuggery-Shooting__Army-Cyber.png",
    ], [
"""<p><strong>Slovakija, Bratislava (2009 m.)</strong> KKP atstovai stūmė Falun Dafa praktikuojančią ponią Su daugiau kaip 20 laiptelių žemyn. Atsitrenkusi galva į žemę ji buvo sužeista ir kraujavo. Slovakijos žiniasklaidos žurnalistai po smurtinio užpuolimo apklausė praktikuotojus.</p>""",
"""<p><strong>David Lian, Pietų Afrika (2004 m.)</strong> 2004 m. Pietų Afrikos Respublikoje buvo užpultas Australijos gydytojas Davidas Lianas, kai bandė pateikti apeliaciją per Bo Sjilaiso vizitą. Važiuodamas automobiliu greitkeliu jis buvo apšaudytas iš galo iš AK47 šautuvų. Buvo pataikyta į automobilio padangas ir Liano kojas.</p>""",
"""<p><strong>Dokumentinis filmas: Po šūvių</strong> Dokumentinis filmas apie tai, kas lėmė išpuolį, ir apie stebuklingą ir greitą Deivido Liano pasveikimą. Žiūrėkite per Faluninfo.TV</p>""",
"""<p><strong>Niujorkas, Flushing (2008 m.)</strong> Flushingo gyventojas Edmondas Erhas buvo užpultas KKP šalininkų gaujos, kai palaikė išstojimą iš Kinijos komunistų partijos skatinantį stendą. (2008 m. liepos 10 d.)</p>""",
"""<p><strong>Johnas Liu, Niujorko tarybos narys; Kinijos generalinis konsulas Peng Keyu</strong> Niujorko tarybos narys Džonas Liu (kairėje) buvo toje vietoje, kur didelės grupės Kinijos KKP šalininkų užpuolė Falun Dafa pasekėjus Flushingo mieste. Kinijos generalinis konsulas Peng Keyu (dešinėje) per pokalbį telefonu su WOIPFLG tyrėju prisipažino, kad Kinijos konsulatas dalyvavo vykdant neapykantos nusikaltimus prieš Falun Dafa praktikuojančiuosius Flushinge, Niujorke.</p>""",
"""<p><strong>Flushinge, Niujorke, suimtas KKP narys (2008 m.)</strong> Policija suėmė KKP narį, užpuolusį Falun Dafa praktikuotoją Flushinge, Niujorke, 2008 m. gegužės 30 d.</p>""",
"""<p><strong>Honkongas (2019 m.)</strong> Liao Qiulan po to, kai jį užpuolė du vyrai su ilgais, į lazdą panašiais daiktais, o kitas vyriškis mušimą įrašinėjo. Tada vyrai įšoko į baltą automobilį ir nuvažiavo. Incidentas įvyko tą dieną, kai Falun Dafa rengia kasmetines eitynes, kuriomis siekiama demaskuoti vykstantį persekiojimą.</p>""",
"""<p><strong>Peter Yuan Li, Atlanta (2006 m.)</strong> Po to, kai "Forbes" išspausdino naujieną apie praktikuotojų specialistų sėkmę įveikiant Kinijos interneto blokadą, trys banditai įsiveržė į Li Juano, vieno iš pagrindinių technikų, namus. Uždengė jam akis lipnia juosta, sumušė ir pavogė kompiuterius. Po užpuolimo jam liko penkiolika siūlių ant galvos.</p>""",
"""<p><strong>San Francisko, kinų kvartalas (2012 m.)</strong> 2012 m. birželio 10 d. San Francisko kinų kvartale grupė KKP banditų užpuolė Falun Dafa praktikuotojus Van Dafaną ir Dereką Vaną. Dereką Vaną užpuolė banditas Vu JonJao.</p>""",
"""<p><strong>Argentina</strong></p>""",
"""<p><strong>CCP kariuomenės kibernetinės atakos (2011 m.)</strong> 2011 m. liepos mėn. per CCTV transliuotoje laidoje buvo parodytos Liaudies išlaisvinimo armijos kibernetinės atakos prieš su Falun Dafa susijusias interneto svetaines, tokias kaip FalunDafa.org, Minghui.org ir kt.</p>""",
    ]
)

exercise_picture_directives = declare_rotation(
    "exercise_pictures",
[
    "/Falun-Gong-Dafa-Exercises_E1__1.0-1.png",
    "/Falun-Gong-Dafa-Exercises_E2__1.0.png",
    "/Falun-Gong-Dafa-Exercises_E3__1.0.png",
    "/Falun-Gong-Dafa-Exercises_E4__1.0.png",
    "/Falun-Gong-Dafa-Exercises_E5a__1.0.png",
], [
    "Buda ištiesia tūkstantį rankų",
    "Falun stovėjimo pratimas",
    "Prasiskverbti į kosminius pakraščius",
    "Falun dangaus ratas",
    "Stebuklingų galių stiprinimas",
])
around_the_world_directives = declare_rotation(
    "around_the_world",
[
    "/rotate2/resized_2018-5-21-mh-paris-dafaday-01_kac1Eai-scaled.jpg",
    "/rotate2/resized_WASHINGTON-DC-20140717_EdwardDai_032.jpg",
    "/rotate2/resized_2006-2-11-italy-11.jpg",
    "/rotate2/resized_2011-4-25-minghui-taiwan-425-02.jpg",
    "/rotate2/resized_around-the-world-sydney-australia.jpg",
    "/rotate2/resized_around_the_world_india.png",
    "/rotate2/resized_2017-5-10-canada-ottawa_03.jpg",
    "/rotate2/resized_2012-5-31-falun-gong-russia-04.jpg",
    "/rotate2/resized_2012-4-18-cmh-japan-2.jpg",
    "/rotate2/resized_20150513-FalunDafa-SamiraBouaou-9647.jpg",
    "/rotate2/resized_2018-7-26-germany-parade-720_01.jpg",
    "/rotate2/resized_2018-9-28-prague-group-practice_06_haQdCdN.jpg",
    "/rotate2/resized_2002-3-29-hellen_practice.jpg",
    "/rotate2/resized_FD__EMP9125_WEB.jpg",
], [
    "Paryžius, Prancūzija",
    "Vašingtonas DC, JAV",
    "Šiaurės, Italija",
    "Taipėjus, Taivanas",
    "Sidnėjus, Australija",
    "Indija",
    "Otava, Kanada",
    "Maskva, Rusija",
    "Hirošima, Japonija",
    "Bruklinas Niujorke, JAV",
    "Berlynas, Vokietija",
    "Praha, Čekija",
    "Atėnai, Graikija",
    "New South Whales, Australija",
])



health_expo_directives = declare_rotation(
    "health_expo",
[
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo-2-1024x576.jpg",
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo3-1024x576.jpg",
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo_Patient-on-back.jpg",
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo2-1-1024x576.jpg",
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo_Autographs.jpg",
    "/Falun-Gong-Story_Rise-of-Falun-Gong_Master-Li-Healing-Asia-Expo_Awards.jpg",
], [
    """<p>
    <strong>Li Hongzhi teikia cigungo terapiją Azijos sveikatos parodos dalyviams 1992 m.</strong>
Parodoje ponas Li Hundži ir jo mokiniai naudojo savo gydomuosius gebėjimus, kad gydytų žmones. Žinia „Falun Gong yra stebuklingas” greitai pasklido tarp parodoje susirinkusių žmonių.
    <p>
    """,
    """<p>
    <strong>Li Hongzhi teikia cigungo terapiją Azijos sveikatos parodos dalyviams 1992 m.</strong>
Vyriausiasis parodos vadovas Li Rusonas sakė: „Parodoje Falungun sulaukė didžiausio pripažinimo, ir jo gydymo rezultatai buvo geri.” Parodos generalinis patarėjas profesorius Dzan Sjuegui sakė: „Būdamas parodos generalinis patarėjas, imuosi atsakomybės ir visiems rekomenduoju Falungun. Tikiu, kad ši praktika tikrai atneš žmonėms gerą sveikatą ir naują dvasinį požiūrį."
    <p>
    """,
    """<p>
    <strong>Atvežti pacientą gydyti pas poną Li.</strong>
Kaip dokumentiniame filme "Dabar ir ateičiai" (Now And For The Future) prisimena parodos darbuotoja ponia Jan Li, „Pagalvojau, argi nebūtų puiku, jei šią akimirką galėtų įrašyti televizijos stotis? Šis žmogus buvo išgydytas per kelias sekundes. Tada pamačiau, kaip vidutinio amžiaus vyras, tikriausiai jos sūnus, akimirksniu atsiklaupė priešais poną Li. Jis taip pat buvo labai susijaudinęs, juk tai buvo jo motina, kuri buvo išgydyta. Ponas Li tarė tam vyrui: "Kelkis, kelkis, kelkis", tarsi tai, ką jis padarė, būtų buvę niekas. Taip ir buvo. Todėl neaišku, kiek tokių žmonių kaip ji jis išgydė per dieną”.
    <p>
    """,
    """<p>
    <strong>Parodos pacientas atsiklaupia ant kelių ir dėkoja ponui Li už išgydymą.</strong>
Buvo ir keletas paralyžiuotų pacientų, kurie vietoje atsistojo iš vežimėlių, nusimetė lazdas ir ėjo pirmyn. Taigi tą akimirką kai kurie žmonės atsiklaupė prieš poną Li ir išreiškė savo dėkingumą.
    <p>
    """,
    """<p>
    <strong>Parodos dalyviai, prašantys pono Li autografo ant Falungun knygos.</strong>
1993 m., kai tik buvo atidarytas parodų centras, daugybė žmonių nubėgo prie Falungun stendo ir sudarė tris eiles. Viena jų buvo skirta užsiregistruoti į rytinę gydymo sesiją, kita - į popietinę sesiją, o trečioji - gauti pono Li autografą ant Falungun knygos.
    <p>
    """,
    """<p>
    <strong>Falungun ir ponas Li gavo aukščiausius apdovanojimus.</strong>
1992 m. ir 1993 m. Azijos sveikatos parodoje "Asia Health Expo" Falungun ir pono Li gydymo procedūros tapo labiausiai ieškomu reiškiniu. Parodoje ponas Li gavo daugiausiai ir aukščiausių apdovanojimų: "Apdovanojimas už pažangius mokslus", "Specialusis aukso prizas" ir "Pripažįstamiausio ciguno meistro" titulas.
    <p>
    """,
])


spreading_publicly_rotation = declare_rotation(
        "spreading_publicly",
        [
            "/Falun-Gong-Story_Rise-of-Falun-Gong_Exercises-Public-1024x576.jpg",
            "/Falun-Gong-Story_Rise-of-Falun-Gong_Teaching-Falun-Gong-on-the-street-1024x576.jpg",
        ], [
    """<p>
    <strong>Ši praktika tapo tokia populiari</strong>
, kad kiekvieną rytą parkuose ir aikštėse, nuo mažų kaimelių iki didelių miestų, buvo galima pamatyti šimtus ar net tūkstančius žmonių, kurie, skambant ramiai muzikai, atlikdavo Falun Gong meditacinius pratimus prieš eidami į darbą.
    <p>
    """,
    """<p>
    <strong>Spartus plitimas viešumoje.</strong>
 Savanoris moko praeivius gatvėje rankų judesių. Kiekvienas, norintis išmokti penkių paprastų Falungun pratimų, gali jų išmokti nemokamai. Falungun paskaitų lankytojai pradėjo jų mokyti savo šeimas ir draugus. Falungun praktikuotojų skaičius ėmė sparčiai augti.
    <p>
    """,
        ]
)

