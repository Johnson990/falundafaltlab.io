import subprocess
from datetime import datetime

def notify_signal(message, recipients=["+37061722581"]):
    try:
        time = datetime.now()
        with open("contact_log.txt", "a") as f:
            f.write(f"{str(time)} | {message}\n\n")
    except:
        logging.warn(f"failed to write to message log -- {message}")
    try:
        subprocess.run(["signal-cli", "-a", "+37061722581", "send", "-m", message, *recipients])
    except:
        logging.warn(f"failed to send signal -- {message}")

