#!/usr/bin/env python3
import json
from sys import stderr
from head import methods
from helpers import read

def log(*args, **kwargs):
    print(*args, **kwargs, file=stderr, flush=True)

method_registry = {f.__name__: f for f in methods}
order_registry = {}

class BaseServerException(Exception):
    @classmethod
    def cast(cls, error):
        if isinstance(error, cls):
            return error
        else:
            return cls(str(error))

    def details(self):
        return str(self).replace('"', '\\"')

class SerializationError(BaseServerException):
    pass

class InternalServerError(BaseServerException):
    pass

class BadMethod(BaseServerException):
    pass

def raise_error(err):
    error = BaseServerException.cast(err)
    response_serialized = """
        {"method": "error", "body": {"code": "{}", "error", "{}"}}
    """.format(type(error).__name__, error.details())
    print(response_serialized, flush=True)

while True:
    try:
        log("waiting")
        input_data = input()
        log("got input", input_data)
        message = json.loads(input_data)
        log("got a message", message)
        if message["method"] in method_registry:
            method = "load_html"
            registry = method_registry
        elif message["method"] in order_registry:
            method = "executive_order"
            registry = order_registry
        else:
            raise_error(BadMethod("Method not found in backend registry"))
            continue
        response = {
            "method": method,
            "body": registry[message["method"]](**message["body"]),
        }
    except Exception as err:
        raise_error(InternalServerError(str(err)))
        continue
    try:
        response_serialized = json.dumps(response)
        print(response_serialized, flush=True)
    except Exception as err:
        raise_error(SerializationError(str(response)))
