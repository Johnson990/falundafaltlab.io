from typing import Dict, List
style_sheet = """
.acordeon {
    background: wheat;
}
.acordeon div {
    width: 100%;
}
.acordeon span {
    float: right;
    user-select: none;
    font-size: large;
    padding: 0px 3px 0px 3px;
    margin-top: -3px;
    transition: transform 2s;
}
.folded_box {
    transform: scale(1, 0);
    max-height: 0px;
    overflow: hidden;
    transition: all 0.2s ease-out;
}
.unfolded_box {
    max-height: 200px;
    transition: all 0.2s ease-in;
}
.rotate45 {
    transform: rotate(45);
    transition: transform 2s;
}
"""
acordeon_template = """
<div id="acordeon_{aid}" class="acordeon">
{jinja_please}
</div>
"""
i_should_obviously_use_jinja = """
<div id="acordeon_{aid}_{qid}_toggle">
<h4>{question}<span>✚</span></h4>
</div>
<div id="acordeon_{aid}_{qid}" class="folded_box">
<p>{answer}</p>
</div>
"""

acordeon_states = {}
def get_id(name):
    if name not in acordeon_states:
        return name
    return name + str(next(filter(lambda i: i not in acordeon_states, range(1, 100000))))

def declare_acordeon(name, questions: Dict[str, List[str]]):
    aid = get_id(name)
    acordeon_states[aid] = {qid: False for qid in range(len(questions))}
    frames = [
        i_should_obviously_use_jinja.format(aid=aid, qid=i, question=qa[0], answer="</p><p>".join(qa[1]))
        for i, qa in enumerate(questions.items())
    ]
    jinja_please = "\n".join(frames)
    html = acordeon_template.format(aid=aid, jinja_please=jinja_please)
    event_listeners = [
        {"mode": "click", "specifier": f"acordeon_{aid}_{qid}_toggle", "value": ("toggle_acordeon", {"aid": aid, "qid": qid})}
        for qid in range(len(questions))
    ]
    return [
        {"mode": "replace", "specifier": name, "value": html},
        *event_listeners,
    ]

def toggle_acordeon(aid, qid):
    acordeon = acordeon_states[aid]
    acordeon[qid] = not acordeon[qid]
    return [
        {"mode": "className", "specifier": f"acordeon_{aid}_{qid}", "value": "unfolded_box" if acordeon[qid] else "folded_box"},
        {"mode": "className", "specifier": f"acordeon_{aid}_{qid}_toggle.0.0", "value": "rotate45" if acordeon[qid] else None},
    ]
