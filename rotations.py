from settings import CONTENT_SERVER

style_sheet = """
.rotating_images {
    display: flex;
    flex-flow: column wrap;
    background: wheat;
}
.rotating_images div {
    display: flex;
}
.rotating_images div > div {
    width: 50%;
    justify-content: center;
    user-select: none;
}
.rotating_images figure {
    width: 100%;
    height: 0px;
    padding: 0px;
    margin: 0px;
    overflow: visible;
}
.rotating_images img {
    padding: 0px;
    max-width: 100%;
}
.faded_box {
    opacity: 0;
    transition: opacity 1s linear;
}
.visible_box {
    object-fit: contain;
    transition: opacity 1s linear;
}
#rotating_image__meta {
    opacity: 0;
    object-fit: contain;
    width: 100%;
    height: unset;
}
.rotating_images span {
    padding: 3px;
}
"""
rotator_template = """
<div class="rotating_images">
<div>
<div id="picture_rotate_{rid}_prev"> ⇦ </div>
<div id="picture_rotate_{rid}_next"> ⇨ </div>
</div>
{jinja_please}
<span id="rotating_image_{rid}_caption">{caption}</span>
</div>
"""
i_should_obviously_use_jinja = """
<figure id="rotating_image_{rid}_{img_id}" class="faded_box">
<img src="{img_location}">
</figure>
"""

rotator_states = {}
def get_id(name):
    if name not in rotator_states:
        return name
    return name + str(next(filter(lambda i: i not in rotator_states, range(1, 100000))))

def declare_rotation(name, images, captions):
    if len(images) != len(captions):
        raise Exception("list of images should equal list of captions")
    rid = get_id(name)
    rotator_states[rid] = dict(
        current_image=0,
        captions=captions.copy(),
    )
    frames = [
        i_should_obviously_use_jinja.format(rid=rid, img_id=i, img_location=CONTENT_SERVER + img_location)
        for i, img_location in enumerate(images)
    ]
    frames.append(
        i_should_obviously_use_jinja
            .format(rid="", img_id="meta", img_location=CONTENT_SERVER + images[0])
            .replace("faded_box", "meta_placeholder", 1)
            #  .replace(f'rotating_image_{rid}_{img_id}" class="faded_box', "meta_placeholder", 1)
    )
    jinja_please = "\n".join(frames).replace("faded_box", "visible_box", 1)
    html = rotator_template.format(rid=rid, jinja_please=jinja_please, caption=captions[0])
    return [
        {"mode": "replace", "specifier": name, "value": html},
        {"mode": "click", "specifier": f"picture_rotate_{rid}_prev", "value": ("picture_rotator", {"rid": rid, "direction": -1})},
        {"mode": "click", "specifier": f"picture_rotate_{rid}_next", "value": ("picture_rotator", {"rid": rid, "direction": 1})},
    ]

def picture_rotator(rid, direction):
    rotator = rotator_states[rid]
    old_image = rotator["current_image"]
    new_image = (old_image + direction) % len(rotator["captions"])
    rotator["current_image"] = new_image
    new_caption = rotator["captions"][new_image]
    return [
        {"mode": "className", "specifier": f"rotating_image_{rid}_{old_image}", "value": "faded_box"},
        {"mode": "className", "specifier": f"rotating_image_{rid}_{new_image}", "value": "visible_box"},
        {"mode": "replace", "specifier": f"rotating_image_{rid}_caption", "value": new_caption},
    ]
